<?php
$names = array(
    'Luke',
    'Han',
    'Leia',
    'Ben',
);

$arr_names = array();
$arr_names[0] = 'Ben';
$arr_names[1] = 'Luke';
$arr_names[2] = 'Han';
$arr_names['Organa'] = 'Leia';

$luke = array(
    'nom' => 'Skywalker',
    'prenom' => 'Luke',
    'job' => 'unknow',
);
$ben = array(
    'nom' => 'Kenobi',
    'prenom' => 'Ben',
    'job' => 'Old man euh.. old jedi master',
);
$han = array(
    'nom' => 'Solo',
    'prenom' => 'Han',
    'job' => 'smuggler',
);

$chars = array(
    $luke,
    $ben,
    $han,
);