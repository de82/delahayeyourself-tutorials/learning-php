<?php

$lignes = file('dwarfs.csv');

$dwarfs = array();

foreach($lignes as $ligne){
    $valeurs = explode(';', $ligne);
    $dwarfs[$valeurs[0]] = $valeurs[1];
}

do {

    foreach(array_keys($dwarfs) as $index => $name){
        printf('%s -> %s,%s', $index, $name, PHP_EOL);
    }

    printf("%s\t> Votre choix ? (q, quit ou exit pour quitter): ", PHP_EOL);
    $saisie = readline();
    if(filter_var($saisie, FILTER_VALIDATE_INT)){
        $cle = intval($saisie);

        if(array_key_exists($cle, array_keys($dwarfs))){
            $dwarf_index = array_keys($dwarfs)[$cle];
            $description = trim($dwarfs[$dwarf_index]);
            printf("Nom: %s,%sDescription: %s%s", $dwarf_index, PHP_EOL, $description,  PHP_EOL);
        }else{
            printf('Invalid entry!%s', PHP_EOL);
        }
        print('Press any key to continue');
        readline();
    }
    

}while($saisie != 'q' 
       && $saisie != 'exit' 
       && $saisie != 'quit');