<?php


function write($string){
    print($string);
}

function writeline($string){
    printf('%s%s', $string, PHP_EOL);
}

function readint(){
    $user_value = readline();
    return intval($user_value);
}