<?php

print('Your firstname: ');
$firstname = readline();

printf('Len of %s: %s%s', $firstname, strlen($firstname), PHP_EOL);
printf('Replace e by a in %s: %s%s', $firstname, str_replace('e', 'a', $firstname), PHP_EOL);

printf('Shuffle %s: %s%s', $firstname, str_shuffle($firstname), PHP_EOL);


printf('lower %s: %s%s', $firstname, strtolower($firstname), PHP_EOL);
printf('upper %s: %s%s', $firstname, strtoupper($firstname), PHP_EOL);