<?php

function say_hello($name=null){
    if ($name == null){
        $name = 'John Doe';
    }
    $sentence = sprintf('Hello %s!', $name);
    //printf('%s%s', $sentence, PHP_EOL);
    return $sentence;
}

function print_array_values($arr){
    foreach($arr as $value){
        writeline($value);
    }
}

function print_array($arr){
    foreach($arr as $key => $value){
        $str = sprintf("Key %s: %s", $key, $value);
        writeline($str);
    }
}

function search($arr, $key){
    if (array_key_exists($key, $arr)){
        return $arr[$key];
    }
    return null;
}