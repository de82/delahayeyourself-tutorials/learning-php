<?php

// on inclu les fichiers de fonctions dont on a besoin
require_once('../tp2_functions/cli.php');
require_once('db_functions.php');

// Appel de connect pour créer la connexion à la BDD
$db = connect('keyforge.sqlite3');


function search($db){
    /*
    Search permet de rechercher une carte par son nom
    */
    $query = 'SELECT name, power, aember FROM cards WHERE name LIKE ?';

    // On récupérer le nom de la carte dans la console et on y ajoute % devant et derrière
    write('Card to search: ');
    $name = readline();
    $name = sprintf('%%%s%%', $name);

    //Execution de la requete
    $rows = select($query, array('name', 'power', 'aember'), $db, array($name));

    //On parcours les résultat et on affiche nom, puissance et aember
    foreach($rows as $row){
        $message = sprintf('Name: %s, power: %s, aember: %s', $row['name'], $row['power'], $row['aember']);
        writeline($message);
    }
}


function insert($params, $db){
    /*
    insert permet l'insertion d'une nouvelle ligne dans la table cards (seulement nom, puissance et aember pour le moment à vous de rajouter les autres colonnes)
    */
    $insert_query = 'INSERT INTO cards (name, power, aember) VALUES (?,?,?)';
    query($insert_query, $params, $db);
}


insert(array('Young Bruno', 2, 1), $db);
search($db);




/*
$query = 'SELECT * FROM cards LIMIT 1;';



//var_dump($rows);

$rows = select('SELECT * FROM houses', array('name', 'id'), $db);

foreach($rows as $row){
    //writeline(sprintf('Name: %s', $row['name']));
}

$rows = select('SELECT COUNT(id) AS total from cards', array('total'), $db);

writeline(sprintf('total cards: %s', $rows[0]['total']));


$query = 'SELECT cards.name AS name, types.name as type, houses.name as house, traits.name as trait FROM cards, types, houses, cards_traits, traits WHERE cards.type_id = types.id AND cards.house_id = houses.id AND cards_traits.card_id = cards.id AND cards_traits.trait_id = traits.id LIMIT 1';

$rows = select($query, array('name', 'type', 'house', 'trait'), $db);

var_dump($rows);*/