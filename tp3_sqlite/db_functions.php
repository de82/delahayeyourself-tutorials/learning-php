<?php

function connect($filepath){
    /*
    connect prend un chemin en paramètre et retourne un "objet" de type pdo qui contient la connexion vers
    la base de données
    arrête l'exécution si la connexion est impossible
    @filepath: chemin vers le fichier sqlite pour créer la connexion
    */

    $conn_path = sprintf('sqlite:%s', $filepath);
    // On crée le chemin de connexion à la base de données en ajoutant sqlite: devant le chemin vers le fichier sqlite


    try {
        //Tentative de connexion à la BDD
        $db = new PDO($conn_path);
        //On modifie le fonctionnement interne de PDO, notamment la gestion des erreurs
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // Retourne la connexion à la BDD
        return $db;
    }
    catch(Exception $e){
        // Si la connexion échoue alors on arrête l'exécution via l'instruction exit et on affiche le message d'erreur
        exit(sprintf('Error: %s', $e->getMessage()));
    }
}


function select($query, $cols, $db, $params=NULL){
    /*
    select permet d'executer une requête de selection sur une BDD avec et sans paramètres
    @query: la requête SQL à exécuter sur la BDD (string)
    @cols: les colonnes à retourner depuis les enregistrement de la BDD (array)
    @db: la connexion à la BDD (pdo)
    @params: peut être NULL, les paramètres à exécuter sur la requete
    */

    // On prépare la requête et on obtient une requête PDO
    $db_query = $db->prepare($query);

    // Si paramètres fournis alors execution avec params sinon sans params
    if($params == NULL){
        $db_query->execute();
    }else{
        $db_query->execute($params);
    }

    //Création d'un tableau vide qui accueillera différents tableau qui représenteront chacun un enregistrement de la BDD
    $rows = array();

    //Parcours des enregistrement après execution de la requête
    while($data = $db_query->fetch()){
        //création d'un tableau vide qui correspond à l'enregistrement courant
        $row = array();
        //pour chaque colonne passé dans $cols, on récupére la valeur dans $data et on l'ajoute au tableau $row
        foreach($cols as $col){
            $row[$col] = $data[$col];
        }
        //on ajoute "l'enregistrement" à notre tableau d'enregistement
        $rows[] = $row;
    }
    //on ferme le curseur de la requête
    $db_query->closeCursor();
    //on retourne les enregistrements.
    return $rows;
}

function query($query, $params, $db){
    /*
    query permet d'exécuter une requete sur la BDD, il s'agit d'une requête qui ne retourne pas de résultat
    @query: la requete sql (string)
    @params: les params à utiliser pour la requête (array)
    @db: la connexion (pdo)
    */
    $db_query = $db->prepare($query);
    $db_query->execute($params);
    $db_query->closeCursor();
}